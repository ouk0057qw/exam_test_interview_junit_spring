package com.ascendcorp.exam.model;

import java.util.Date;

public class TransferResponse {


    private String responseCode;
    private String description;
    private String referenceCode1;
    private String referenceCode2;
    private Double amount;
    private String bankTransactionID;

    public TransferResponse() {
    }

    public TransferResponse(
            String responseCode,
            String description,
            Double amount,
            String bankTransactionID,
            String referenceCode1,
            String referenceCode2) {
        this.responseCode = responseCode;
        this.description = description;
        this.referenceCode1 = referenceCode1;
        this.referenceCode2 = referenceCode2;
        this.amount = amount;
        this.bankTransactionID = bankTransactionID;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReferenceCode1() {
        return referenceCode1;
    }

    public String getReferenceCode2() {
        return referenceCode2;
    }

    public Double getBalance() {
        return amount;
    }

    public String getBankTransactionID() {
        return bankTransactionID;
    }

    public void setReferenceCode1(String referenceCode1) {
        this.referenceCode1 = referenceCode1;
    }

    public void setReferenceCode2(String referenceCode2) {
        this.referenceCode2 = referenceCode2;
    }

    public void setBalance(double amount) {
        this.amount = amount;
    }


    public void setBankTransactionID(String bankTransactionID) {
        this.bankTransactionID = bankTransactionID;
    }

}
